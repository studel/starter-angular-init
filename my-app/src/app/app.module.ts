import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { SharedModule } from './shared/shared.module';

// import { environment } from '../environments/environment';

import { ItemsService } from './core/services/items/items.service';

import { AppComponent } from './app.component';

import { environment } from '../environments/environment.prod';

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgbModule.forRoot(),
    CoreModule,
    SharedModule,
    AppRoutingModule,
    HomeModule,
    // ItemsModule, (lazy loading)
    PageNotFoundModule,
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
   constructor(router: Router) {
     if (!environment.production) {
       console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
     }
   }
}
