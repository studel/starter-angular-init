import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from '../page-not-found/components/page-not-found/page-not-found.component';

const homeRoutes: Routes = [
  { path: 'home', component: HomeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      homeRoutes
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
