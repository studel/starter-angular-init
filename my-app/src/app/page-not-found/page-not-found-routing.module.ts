import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

const pageNotFoundRoutes: Routes = [
  { path: '**', component: PageNotFoundComponent },
];


@NgModule({
  imports: [
    CommonModule ,
    RouterModule.forChild(
      pageNotFoundRoutes
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class PageNotFoundRoutingModule { }
