import { Item } from '../../shared/models/item.model';

export const ITEMS: Item[] = [
  {
    id: 'stu',
    name: 'Stéphane',
    reference: '1234',
    state: 'A livrer'
  },
  {
    id: 'oma',
    name: 'omar',
    reference: '5678',
    state: 'En cours'
  },
  {
    id: 'gab',
    name: 'gabrielle',
    reference: '9012',
    state: 'Livrée'
  }
];
