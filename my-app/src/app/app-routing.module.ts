import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { HomeComponent } from './home/components/home/home.component';
import { ListItemsComponent } from './items/containers/list-items/list-items.component';

const appRoutes: Routes = [
  {
    path: 'items', loadChildren: 'app/items/items.module#ItemsModule' // lazy loading
  },

  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      {preloadingStrategy: PreloadAllModules},
      // { enableTracing: true } // <-- debugging purposes only
    )],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
