import { Directive, Input, HostBinding } from '@angular/core';
import { State } from '../../enums/state.enum';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit {
  @Input('appState') appState: State;
  @HostBinding('class') elementClass: string;

  constructor() { }

  ngOnInit(): void {
    console.log(this.appState);
    this.elementClass = this.formatClass(this.appState);
  }

  private removeAccent(state: string): string {
    return state.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

  }

  private formatClass(state: string): string {
    return `state-${(this.removeAccent(state)).toLowerCase().replace(' ', '-')}`;
  }

}
